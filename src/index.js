import _ from 'lodash';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import YTSearch from 'youtube-api-search';
import SearchBar from './components/search_bar';
import VideoDetail from './components/video_detail';
import VideoList from './components/video_list';

const API_KEY = 'AIzaSyDA9vse1f69MYzklC7erOta9J1Jx_lagxE';

class App extends Component {

    /**
     * 
     * @param {*} props 
     */
    constructor(props) {
        super(props);
        this.state = { 
            videos: [], 
            selectedVideo: null 
        };
        this.videoSearch('surfboards');
    }

    /**
     * 
     */
    render() {
        const videoSearch = _.debounce(term => this.videoSearch(term), 300);
        return (
            <div>
                <SearchBar onSearchTermChange={videoSearch}/>
                <VideoDetail video={this.state.selectedVideo} />
                <VideoList 
                    onVideoSelect={selectedVideo => this.setState({selectedVideo})}
                    videos={this.state.videos} />
            </div>
        );
    } 
    
    /**
     * 
     * @param {*} term 
     */
    videoSearch(term) {
        YTSearch({key: API_KEY, term: term}, videos => this.setState({
            videos, 
            selectedVideo: videos[0]
        }));
    }
};

ReactDOM.render(<App/>, document.querySelector('.container'));